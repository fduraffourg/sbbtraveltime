{-# LANGUAGE OverloadedStrings #-}

module Data.Journey
  ( Journey(..)
  , createJourney
  , quickestJourney
  ) where

import Data.Aeson
import qualified Data.ByteString as B
import Data.GTFS
import Data.Maybe
import Data.Point
import Data.Schedule
import qualified Data.Text as T
import Data.Text.Encoding (decodeUtf8)

data Journey = Journey
  { dstName :: T.Text
  , dstStopID :: StopID
  , dstLat :: Double
  , dstLon :: Double
  , duration :: Int
  } deriving (Show)

instance ToJSON Journey where
  toJSON (Journey dstName _ lat lon duration) =
    object
      [ "destination_name" .= dstName
      , "destination_lat" .= lat
      , "destination_lon" .= lon
      , "duration" .= duration
      ]

createJourney :: Schedule -> Point -> Point -> Journey
createJourney sch po pd = Journey (name pd) dstStopID dstLat dstLon duration
  where
    name =
      decodeUtf8 . fromMaybe "" . fmap stopName . getStopByID sch . pointStopID
    dstStopID = stopID stopDst
    duration =
      (flip quot 60) $ durationBetweenInstants (pointTime po) (pointTime pd)
    dstLat = stopLat stopDst
    dstLon = stopLon stopDst
    stopDst = fromMaybe undefined $ getStopByID sch $ pointStopID pd

quickestJourney :: Journey -> Journey -> Journey
quickestJourney ja jb =
  if (duration ja < duration jb)
    then ja
    else jb
