{-# LANGUAGE OverloadedStrings #-}

module Data.GTFS
  ( StopID(..)
  , TripID(..)
  , Instant(..)
  , Stop(..)
  , StopTime(..)
  , Trip
  , durationBetweenInstants
  ) where

import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as BSC
import Data.Csv
import Data.Hashable
import Data.Maybe

newtype StopID =
  StopID B.ByteString
  deriving (Eq, Show)

instance Hashable StopID where
  hashWithSalt i (StopID bs) = hashWithSalt i bs

newtype TripID =
  TripID B.ByteString
  deriving (Eq, Show)

newtype Instant =
  Instant Int
  deriving (Eq, Show, Ord)

instance Hashable TripID where
  hashWithSalt i (TripID bs) = hashWithSalt i bs

data Stop = Stop
  { stopID :: !StopID
  , stopName :: !B.ByteString
  , stopParent :: !StopID
  , stopLat :: !Double
  , stopLon :: !Double
  } deriving (Show)

instance FromNamedRecord Stop where
  parseNamedRecord r =
    Stop <$> (StopID <$> r .: "stop_id") <*> r .: "stop_name" <*>
    (StopID <$> r .: "parent_station") <*>
    r .: "stop_lat" <*>
    r .: "stop_lon"

data StopTime = StopTime
  { stTripID :: !TripID
  , stArrivalTime :: !Instant
  , stDepartureTime :: !Instant
  , stStopID :: !StopID
  , stStopSequence :: !Int
  } deriving (Show)

instance FromNamedRecord StopTime where
  parseNamedRecord r =
    StopTime <$> (TripID <$> r .: "trip_id") <*>
    (parseInstant <$> r .: "arrival_time") <*>
    (parseInstant <$> r .: "departure_time") <*>
    (StopID <$> r .: "stop_id") <*>
    r .: "stop_sequence"

type Trip = [StopTime]

parseInstant :: B.ByteString -> Instant
parseInstant s =
  case BSC.split ':' s of
    h:m:s:[] ->
      fromMaybe undefined $ do
        hours <- fst <$> BSC.readInt h
        minutes <- fst <$> BSC.readInt m
        seconds <- fst <$> BSC.readInt s
        return $ Instant $ hours * 3600 + minutes * 60 + seconds
    _ -> undefined

durationBetweenInstants :: Instant -> Instant -> Int
durationBetweenInstants (Instant a) (Instant b) = b - a
