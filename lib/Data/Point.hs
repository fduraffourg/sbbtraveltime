module Data.Point
  ( Point(..)
  , earliestPoint
  ) where

import Data.GTFS

data Point = Point
  { pointStopID :: StopID
  , pointTime :: Instant
  } deriving (Show)

earliestPoint :: Point -> Point -> Point
earliestPoint pa pb =
  if (pointTime pa < pointTime pb)
    then pa
    else pb
