module Data.Schedule
  ( Schedule
  , createSchedule
  , getTripsFromStopAndTime
  , getStopByID
  , getParentStopID
  , getChildStops
  , findStopsByName
  , getStopTimesForStopID
  ) where

import qualified Data.ByteString as B
import Data.GTFS
import qualified Data.HashMap.Lazy as HML
import Data.Maybe
import qualified Data.Vector as V
import Debug.Trace

data Schedule = Schedule
  { stops :: V.Vector Stop
  , stopsByID :: HML.HashMap StopID Stop
  , stByTripID :: HML.HashMap TripID [StopTime]
  , stByStopID :: HML.HashMap StopID [StopTime]
  , childStops :: HML.HashMap StopID [StopID] -- All stops whose parent is the index
  }

createSchedule :: V.Vector Stop -> V.Vector StopTime -> Schedule
createSchedule stops stopTimes =
  Schedule
    { stops = stops
    , stopsByID = indexStopsByID stops
    , stByTripID = indexStopTimesByTripID stopTimes
    , stByStopID = indexStopTimesByStopID stopTimes
    , childStops = indexChildStops stops
    }

------------------------
-- Indexing Functions --
------------------------
indexStopsByID :: V.Vector Stop -> HML.HashMap StopID Stop
indexStopsByID = HML.fromList . fmap (\s -> (stopID s, s)) . V.toList

indexStopTimesByTripID :: V.Vector StopTime -> HML.HashMap TripID [StopTime]
indexStopTimesByTripID =
  HML.fromListWith (++) . fmap (\s -> (stTripID s, [s])) . V.toList

indexStopTimesByStopID :: V.Vector StopTime -> HML.HashMap StopID [StopTime]
indexStopTimesByStopID =
  HML.fromListWith (++) . fmap (\s -> (stStopID s, [s])) . V.toList

indexChildStops :: V.Vector Stop -> HML.HashMap StopID [StopID]
indexChildStops = HML.fromListWith (++) . fmap indexStop . V.toList
  where
    indexStop s = (stopParent s, [stopID s])

--------------------
-- Data retrieval --
--------------------
-- List trips that go through the given StopID at a time later than the given
-- instant
getTripsFromStopAndTime :: Schedule -> StopID -> Instant -> [Trip]
getTripsFromStopAndTime sch stop time =
  fmap lookupTrip . fmap stTripID . filter changeTimeOK . filter isAfterTime $
  stopTimes
  where
    lookupTrip tripID = fromMaybe [] $ HML.lookup tripID (stByTripID sch)
    stopTimes :: [StopTime]
    stopTimes = fromMaybe [] $ HML.lookup stop (stByStopID sch)
    isAfterTime st = (stDepartureTime st) > time
    changeTimeOK st = durationBetweenInstants time (stDepartureTime st) < 1800

getStopByID :: Schedule -> StopID -> Maybe Stop
getStopByID sch i = HML.lookup i $ stopsByID sch

-- Given a StopID return the StopID if it's parent station or its own StopID if
-- no parent is defined
getParentStopID :: Schedule -> StopID -> StopID
getParentStopID sch i =
  fromMaybe i .
  fmap stopID . (>>= flip HML.lookup byID) . fmap stopParent . HML.lookup i $
  byID
  where
    byID = stopsByID sch

getChildStops :: Schedule -> StopID -> [StopID]
getChildStops sch i = fromMaybe [] . HML.lookup i $ childStops sch

findStopsByName :: Schedule -> B.ByteString -> [StopID]
findStopsByName sch name =
  V.toList . fmap stopID . V.filter ((== name) . stopName) . stops $ sch

getStopTimesForStopID :: Schedule -> StopID -> [StopTime]
getStopTimesForStopID sch stopID =
  fromMaybe [] $ HML.lookup stopID $ stByStopID sch
