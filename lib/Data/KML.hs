{-# LANGUAGE OverloadedStrings #-}

module Data.KML
  ( Document(..)
  , Style(..)
  , Placemark(..)
  , writeKML
  ) where

import qualified Data.ByteString as B
import System.IO

data Document = Document
  { docName :: B.ByteString
  , docStyles :: [Style]
  , docPlacemarks :: [Placemark]
  }

data Style = Style
  { styleID :: B.ByteString
  , styleColor :: B.ByteString
  }

data Placemark = Placemark
  { placeName :: B.ByteString
  , placeStyleID :: B.ByteString
  , placeDescription :: B.ByteString
  , placeCoordinates :: B.ByteString
  }

writeKML :: FilePath -> Document -> IO ()
writeKML file doc =
  withBinaryFile file WriteMode $ \handle -> do
    _ <- B.hPutStr handle documentHeader
    _ <- B.hPutStr handle $ mconcat ["<name>", docName doc, "</name>", "\n"]
    _ <- writeItems handle writeStyle $ docStyles doc
    _ <- writeItems handle writePlacemark $ docPlacemarks doc
    _ <- B.hPutStr handle documentFooter
    return ()
  where
    writeItems h writer items =
      mapM_ (\i -> writer h i >> B.hPutStr h "\n") items

writeStyle :: Handle -> Style -> IO ()
writeStyle h (Style styleID color) =
  B.hPutStr h $
  mconcat
    [ "<Style id=\""
    , styleID
    , "\"><LabelStyle><color>"
    , color
    , "</color></LabelStyle><IconStyle><Icon></Icon></IconStyle></Style>"
    ]

writePlacemark :: Handle -> Placemark -> IO ()
writePlacemark h (Placemark name style description coordinates) =
  B.hPutStr h $
  mconcat
    [ "<Placemark><name>"
    , name
    , "</name>"
    , "<styleUrl>#"
    , style
    , "</styleUrl>"
    , "<description>"
    , description
    , "</description>"
    , "<Point><coordinates>"
    , coordinates
    , "</coordinates></Point>"
    , "</Placemark>"
    ]

------------
-- Static --
------------
documentHeader :: B.ByteString
documentHeader =
  "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<kml xmlns=\"http://www.opengis.net/kml/2.2\">\n<Document>"

documentFooter :: B.ByteString
documentFooter = "</Document></kml>"
