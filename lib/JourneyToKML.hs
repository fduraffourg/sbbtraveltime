{-# LANGUAGE OverloadedStrings #-}

module JourneyToKML
  ( convertJourneysToKML
  ) where

import qualified Data.ByteString as B
import Data.ByteString.Conversion
import Data.Journey
import qualified Data.KML as KML
import Data.Text.Encoding (encodeUtf8)
import Numeric

convertJourneysToKML :: B.ByteString -> [Journey] -> KML.Document
convertJourneysToKML startName journeys = KML.Document name styles placemarks
  where
    name = mconcat ["Travel Times from ", startName]
    styles = createKMLStyles
    placemarks = journeyToPlaceMark <$> journeys

createKMLStyles :: [KML.Style]
createKMLStyles = styleFor <$> [0,10 .. 260]
  where
    styleFor n = KML.Style (styleNameForDuration n) (colorForDuration n)

journeyToPlaceMark :: Journey -> KML.Placemark
journeyToPlaceMark journey = KML.Placemark name style description coordinates
  where
    name = "&#9632;"
    style = styleNameForDuration $ duration journey
    description =
      mconcat
        [ encodeUtf8 $ dstName journey
        , " - Travel time: "
        , formatDuration $ duration journey
        ]
    coordinates =
      mconcat
        [toByteString' $ dstLon journey, ",", toByteString' $ dstLat journey]

formatDuration :: Int -> B.ByteString
formatDuration d =
  mconcat [toByteString' hours, "h", toByteString' minutes, "m"]
  where
    (hours, minutes) = quotRem d 60

colorForDuration :: Int -> B.ByteString
colorForDuration duration = mconcat ["ff", toHex n, toHex (255 - n), "00"]
  where
    n = min 255 $ max 0 duration
    toHex n =
      case B.unpack . toByteString' $ showHex n "" of
        a:[] -> B.pack [48, a]
        a:b:[] -> B.pack [a, b]
        _ -> "00"

styleNameForDuration :: Int -> B.ByteString
styleNameForDuration duration = mconcat ["style-", toByteString' n]
  where
    n = min 25 (quot duration 10)
