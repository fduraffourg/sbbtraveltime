{-# LANGUAGE OverloadedStrings #-}

module Main where

import qualified Codec.Archive.Zip as Zip
import Control.Monad
import qualified Data.Aeson
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as BSC
import qualified Data.ByteString.Lazy as BSL
import Data.Csv
import Data.GTFS
import qualified Data.HashMap.Lazy as HML
import Data.Hashable
import Data.Journey
import qualified Data.KML as KML
import Data.List
import qualified Data.Map as M
import Data.Maybe
import Data.Point
import Data.Schedule
import qualified Data.Vector as V
import JourneyToKML

main :: IO ()
main = do
  let gtfsFile = "gtfs-sbb-reduced.zip"
  stops <- loadStops gtfsFile
  putStrLn $ "Done reading stops: " ++ (show $ length stops)
  stopTimes <- loadStopTimes gtfsFile
  putStrLn $ "Done reading stop_times: " ++ (show $ length stopTimes)
  let indexedSchedule = createSchedule stops stopTimes
  -- let startingPoints = getStartingPoints indexedSchedule "Renens VD"
  -- let start = head startingPoints
  -- putStrLn $ "Starting at:"
  -- putStrLn $ show start
  -- let points = getDestinationsFrom indexedSchedule start
  -- let journeys = fmap (createJourney indexedSchedule start) points
  let startName = "Rheinfelden"
  putStrLn "Starting at:"
  putStrLn $ show startName
  let journeys = getJourneysFromStopName indexedSchedule startName
  putStrLn $ mconcat ["You can reach", show $ length journeys, " destinations"]
  let orderedJourneys = sortOn (duration) journeys
  putStrLn "Save journeys to file"
  _ <- BSL.writeFile "journeys.json" $ Data.Aeson.encode orderedJourneys
  putStrLn "Done"
  -- putStrLn "Creating a KML file"
  -- let kml = convertJourneysToKML startName journeys
  -- _ <- KML.writeKML "travelTimes.kml" kml
  putStrLn "Done"
  return ()

------------
-- Models --
------------
-------------
-- Parsing --
-------------
byteOrderMark = B.pack [0xef, 0xbb, 0xbf]

loadGtfsFile :: FromNamedRecord a => String -> String -> IO (V.Vector a)
loadGtfsFile path file = do
  stopsSelector <- Zip.mkEntrySelector file
  content <- Zip.withArchive path (Zip.getEntry stopsSelector)
  let stripedContent =
        if B.isPrefixOf byteOrderMark content
          then B.drop 3 content
          else content
  let bsLazyStops = BSL.fromStrict stripedContent
  case decodeByName bsLazyStops of
    Left err -> fail err
    Right (_, v) -> return v

loadStops :: String -> IO (V.Vector Stop)
loadStops path = loadGtfsFile path "stops.txt"

loadStopTimes :: String -> IO (V.Vector StopTime)
loadStopTimes path = loadGtfsFile path "stop_times.txt"

----------------------------
-- Indexing and searching --
----------------------------
-- Reduce a Trip to only StopTimes that are after the given Point
tripAfterPoint :: Point -> Trip -> Trip
tripAfterPoint point = drop 1 . dropWhile skip . sortOn stStopSequence
  where
    skip st =
      pointStopID point /= stStopID st || stDepartureTime st < pointTime point

-----------------------
-- Route computation --
-----------------------
-- List all the points that can be used as departure points for the given
-- station name
getStartingPoints :: Schedule -> B.ByteString -> [Point]
getStartingPoints sch origin =
  fmap stopTimeToPoint . (>>= extractStopTimes) $ originIDs
  where
    originIDs = findStopsByName sch origin
    extractStopTimes s = getStopTimesForStopID sch s
    stopTimeToPoint :: StopTime -> Point
    stopTimeToPoint st =
      Point (getParentStopID sch $ stStopID st) (stDepartureTime st)

-- From a given point, give all the possible point that can be reached
getDestinationsFrom :: Schedule -> Point -> HML.HashMap StopID Point
getDestinationsFrom sch start =
  HML.fromListWith earliestPoint .
  fmap indexPoint .
  fmap convertStopTime .
  (>>= tripAfterPoint start) . getTripsFromStopAndTime sch (pointStopID start) $
  (pointTime start)
  where
    convertStopTime st = Point (stStopID st) (stArrivalTime st)
    indexPoint p = (newStopID, p {pointStopID = newStopID})
      where
        newStopID = getParentStopID sch $ pointStopID p

getAllPointsFrom ::
     Schedule
  -> HML.HashMap StopID Point -- Point we already know
  -> Int -- number of iterations left
  -> Point -- Point to start from
  -> HML.HashMap StopID Point
getAllPointsFrom sch initialPoints iteration start =
  if iteration <= 0
    then HML.empty
    else foldl recurseAndUnion directPoints directPoints
  where
    directPoints =
      HML.filter isBetterPoint .
      foldl (HML.unionWith earliestPoint) HML.empty .
      fmap (getDestinationsFrom sch) .
      fmap (\newID -> start {pointStopID = newID}) .
      getChildStops sch . pointStopID $
      start
    newPoints = HML.unionWith earliestPoint initialPoints directPoints
    recurseAndUnion current newPoint =
      HML.unionWith earliestPoint current $
      getAllPointsFrom sch newPoints (iteration - 1) newPoint
    isBetterPoint point =
      case HML.lookup (pointStopID point) initialPoints of
        Just knownPoint -> (pointTime point) < (pointTime knownPoint)
        Nothing -> True

getJourneysFromStopName :: Schedule -> B.ByteString -> [Journey]
getJourneysFromStopName sch name =
  HML.elems .
  HML.fromListWith quickestJourney .
  join . fmap journeysForStart . getStartingPoints sch $
  name
  where
    journeysForStart s =
      fmap (\j -> (dstStopID j, j)) .
      fmap (createJourney sch s) . HML.elems . getAllPointsFrom sch HML.empty 1 $
      s
